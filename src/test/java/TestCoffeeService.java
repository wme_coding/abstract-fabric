import coffee.Coffee;
import factory.AbstractCoffeeFactory;
import factory.BrazilianCoffeeFactory;
import factory.EthiopiaCoffeeFactory;
import org.junit.Assert;
import org.junit.Test;

public class TestCoffeeService {
    @Test
    public void testEthiopiaFabric(){
        AbstractCoffeeFactory factory = new EthiopiaCoffeeFactory();

        Coffee americano = factory.makeAmericano();
        Coffee cappuccino = factory.makeCappuccino();
        Coffee espresso = factory.makeEspresso();

        Assert.assertEquals("EthiopiaAmericano", americano.getClass().getSimpleName());
        Assert.assertEquals("EthiopiaCappuccino", cappuccino.getClass().getSimpleName());
        Assert.assertEquals("EthiopiaEspresso", espresso.getClass().getSimpleName());
    }

    @Test
    public void testBrazilianFabric(){
        AbstractCoffeeFactory factory = new BrazilianCoffeeFactory();

        Coffee americano = factory.makeAmericano();
        Coffee cappuccino = factory.makeCappuccino();
        Coffee espresso = factory.makeEspresso();

        Assert.assertEquals("BrazilianAmericano", americano.getClass().getSimpleName());
        Assert.assertEquals("BrazilianCappuccino", cappuccino.getClass().getSimpleName());
        Assert.assertEquals("BrazilianEspresso", espresso.getClass().getSimpleName());
    }
}
