package factory;

import coffee.Americano;
import coffee.Cappuccino;
import coffee.Espresso;
import ethiopia.EthiopiaAmericano;
import ethiopia.EthiopiaCappuccino;
import ethiopia.EthiopiaEspresso;

public class EthiopiaCoffeeFactory implements AbstractCoffeeFactory{
    @Override
    public Americano makeAmericano() {
        return new EthiopiaAmericano();
    }

    @Override
    public Cappuccino makeCappuccino() {
        return new EthiopiaCappuccino();
    }

    @Override
    public Espresso makeEspresso() {
        return new EthiopiaEspresso();
    }
}
