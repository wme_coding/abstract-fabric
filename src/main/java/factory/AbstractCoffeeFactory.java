package factory;

import coffee.Americano;
import coffee.Cappuccino;
import coffee.Espresso;

public interface AbstractCoffeeFactory {
    Americano makeAmericano();
    Cappuccino makeCappuccino();
    Espresso makeEspresso();
}
