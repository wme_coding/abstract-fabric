package factory;

import brazilian.BrazilianAmericano;
import brazilian.BrazilianCappuccino;
import brazilian.BrazilianEspresso;
import coffee.Americano;
import coffee.Cappuccino;
import coffee.Espresso;

public class BrazilianCoffeeFactory implements AbstractCoffeeFactory{

    @Override
    public Americano makeAmericano() {
        return new BrazilianAmericano();
    }

    @Override
    public Cappuccino makeCappuccino() {
        return new BrazilianCappuccino();
    }

    @Override
    public Espresso makeEspresso() {
        return new BrazilianEspresso();
    }
}
